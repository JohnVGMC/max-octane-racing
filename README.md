# Max Octane Racing
#### David Moreno Ignacio, John Calderon, Alejandro Aquino
#### CS 485 - Game Programming Final Project
#### Professor Xin Ye

Unity Assets for Max Octane Racing, our Final Project for CS 485

### How To Run
By either cloning or downloading the repository, and building the game through Unity.  
Or you can download the game on its own here: [Download Link](https://drive.google.com/open?id=1Pg2mvf24sVWQACbwlINAea04y-dDxpiE).

Default Controls:  
- W = Accelerate/Gas  
- A = Turn Left  
- S = Turn Right  
- D = Reverse  
- Space = Brake  
- Esc = Pause

### Credits
#### David Moreno Ignacio
Practice Track and Level 2 course  
AI Racer Scripting
#### John Calderon
Menu & UI  
Laps, Checkpoint & Timer Scripting
#### Alejandro Aquino
Level 1 course  
Car Controls and Scripting  
Sound and Music