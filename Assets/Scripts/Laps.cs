﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Laps : MonoBehaviour {

	// These Static Variables are accessed in "checkpoint" Script
	public Transform[] checkPointArray;
	public static Transform[] checkpointA;
	public static int currentCheckpoint = 0; 
	public static int currentLap = 0; 
	public Vector3 startPos;
	public int Lap;
	public int MaxLapNumber;

	public Text LapDisplay;
	public Text MaxLapDisplay;

	void  Start ()
	{
		startPos = transform.position;
		currentCheckpoint = 0;
		currentLap = 0; 
		MaxLapDisplay.text = "/" + MaxLapNumber.ToString ();
	}

	void  Update ()
	{
		Lap = currentLap;
		checkpointA = checkPointArray;
		LapDisplay.text = Lap.ToString ();
	}

}
