﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour {

	float destroyAfer = 2f;
	float Timer = 0;

	// Update is called once per frame
	void Update () 
	{
		Timer += Time.deltaTime;
		if(destroyAfer <= Timer)
		{
			Destroy (gameObject);
		}
	}
}
