﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Car_Movement : MonoBehaviour {

	public GameObject Wheel_FR;
	public GameObject Wheel_FL;
	public GameObject Wheel_BR;
	public GameObject Wheel_BL;

	public WheelCollider W_FL;
	public WheelCollider W_FR;
	public WheelCollider W_BL;
	public WheelCollider W_BR;

	public float Torque = 1000f;

	// UI elements
	public Text speedDisplay;
	public RectTransform speedNeedle;

	public float loweststeerspeed = 20f;
	public float loweststeerangle = 40f;
	public float higheststeerangle = 20f;

	public bool isbrake = false; 
	public float brakeTorque = 500000f;

	Rigidbody rb;

	private float carSpeed;

	//braking skin
	public Texture2D textureNormal;
	public Texture2D textureBraking;
	public Renderer carRenderer;
	// Use this for initialization
	void Start () 
	{
		rb = GetComponent<Rigidbody> ();
		Vector3 temp = rb.centerOfMass;
		temp.y = -0.15f;
		rb.centerOfMass = temp;
		speedDisplay.text = "0";
	}
	
	// Update is called once per frame
	void Update () 
	{
		carSpeed = rb.velocity.magnitude * 2.237f;
		// display the speed to the numerical display
		speedDisplay.text = carSpeed.ToString("0");
		// illustrate the speed to the needle display
		float needleAngle = carSpeed * 2.0f;
		if (needleAngle >= 240.0f) // needle cannot exceed the graphic's display angle of 240 (or else it will go past the speedometer)
			needleAngle = 240.0f;
		speedNeedle.rotation = Quaternion.Euler (0, 0, -needleAngle);
	}

	void FixedUpdate()
	{
		CarMovement ();
		RotateWheels();
		steerWheels();
		Handbrake(); 
	}

	void CarMovement()
	{
		W_BL.motorTorque = Torque * Input.GetAxis ("Vertical");
		W_BR.motorTorque = Torque * Input.GetAxis ("Vertical");

		float speedfactor = this.GetComponent<Rigidbody> ().velocity.magnitude / loweststeerspeed;
		float currentsteerAngle = Mathf.Lerp (loweststeerangle, higheststeerangle, speedfactor);

		currentsteerAngle *= Input.GetAxis ("Horizontal");

		W_FL.steerAngle = currentsteerAngle;
		W_FR.steerAngle = currentsteerAngle;
	}

	void RotateWheels ()
	{
		//rotate wheels by using the rpm formula
		Wheel_BL.gameObject.transform.Rotate (W_BL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
		Wheel_BR.gameObject.transform.Rotate (W_BR.rpm / 60 * 360 * Time.deltaTime, 0, 0);
		Wheel_FL.gameObject.transform.Rotate (W_BL.rpm / 60 * 360 * Time.deltaTime, 0, 0);
		Wheel_FR.gameObject.transform.Rotate (W_BR.rpm / 60 * 360 * Time.deltaTime, 0, 0);

	}

	void steerWheels()
	{
		//for the front left wheel of a car
		Vector3 temp;
		temp = Wheel_FL.transform.localEulerAngles;
		temp.y = W_FL.steerAngle;
		Wheel_FL.transform.localEulerAngles = temp;

		//for the front right wheels of a car
		temp = Wheel_FR.transform.localEulerAngles;
		temp.y = W_FR.steerAngle;
		Wheel_FR.transform.localEulerAngles = temp;
	}

	//this function will call the brakes if the spacebar is pressed
	void Handbrake()
	{
		if(Input.GetKey(KeyCode.Space))
			{
				isbrake = true;
			}
		else
			{
				isbrake = false;
			}


		if (isbrake == true) 
		{
			W_BL.brakeTorque = brakeTorque;
			W_BR.brakeTorque = brakeTorque;
			W_BL.motorTorque = 0;
			W_BR.motorTorque = 0;
			carRenderer.material.mainTexture = textureBraking;
		} 
		else 
		{
			W_BL.brakeTorque = 0;
			W_BR.brakeTorque = 0;
			carRenderer.material.mainTexture = textureNormal;
		}

	}
}
