﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkidSound : MonoBehaviour {

	float currentFrictionValue = 0;
	float SkidAt = 0.5f;
	float soundEmission = 5;
	float soundWait = 15;
	GameObject skidSound;
	float markWidth = 0.3f;
	int skidding = 0;

	Vector3[] lastPosition = new Vector3[2];
	public Material skidMaterial;
	
	// Update is called once per frame
	void Update () 
	{
		PlayerSkidSound ();
	}

	void PlayerSkidSound()
	{
		WheelHit hit;
		WheelCollider collider;
		collider = GetComponent<WheelCollider> ();
		collider.GetGroundHit (out hit);
		currentFrictionValue = Mathf.Abs (hit.sidewaysSlip);
		if(SkidAt <= currentFrictionValue && soundWait <= 0)
		{
			skidSound = Instantiate (Resources.Load("SoundSlip"), hit.point, Quaternion.identity) as GameObject;
			soundWait = 1;
		}
		if(SkidAt <= currentFrictionValue)
		{
			SkidMesh ();
		}

		else
		{
			skidding = 0;
		}

		soundWait -= Time.deltaTime * soundEmission;
	}

	void SkidMesh()
	{
		WheelHit hit;
		GetComponent<WheelCollider> ().GetGroundHit (out hit);

		GameObject marks = new GameObject ("Marks");

		marks.AddComponent<MeshFilter>();
		marks.AddComponent<MeshRenderer>();

		//marks the generate
		Mesh markMesh = new Mesh();

		//Vertices for the Cube
		Vector3[] vertices = new Vector3[4];

		int[] triangle = new int[6]{ 0, 1, 2, 2, 3, 0 };

		//these conditions generates the vertices of the cube
		if (skidding == 0) 
		{
			vertices [0] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (markWidth, 0.1f, 0f);
			vertices [1] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (markWidth, 0.1f, 0f);
			vertices [2] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (markWidth, 0.1f, 0f);
			vertices [3] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (markWidth, 0.1f, 0f);

			lastPosition [0] = vertices [2];
			lastPosition [1] = vertices [3]; 
			skidding = 1;
		}

		else 
		{
			vertices[0] = lastPosition [1];
			vertices[1] = lastPosition [0];
	
			vertices [2] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (-markWidth, 0.1f, 0f);
			vertices [3] = hit.point + Quaternion.Euler (transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z) * new Vector3 (-markWidth, 0.1f, 0f);

			lastPosition [0] = vertices [2];
			lastPosition [1] = vertices [3];

		}

			//store all vertices of cube 
			markMesh.vertices = vertices;

			//store vertices of triangle
			markMesh.triangles = triangle;

			//remove corners of triangles and create smoothness
			markMesh.RecalculateNormals();

			//generate the output
			marks.GetComponent<MeshFilter>().mesh = markMesh;
			marks.GetComponent<MeshRenderer> ().material = skidMaterial;

			//destroy marks
			marks.AddComponent<Destroy>();

			
	}
}