﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engine_Sound : MonoBehaviour {

	public float topspeed = 100f;

	private float currentSpeed = 0;
	private float pitch = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentSpeed = transform.GetComponent<Rigidbody> ().velocity.magnitude * 3.6f;
		pitch = currentSpeed / topspeed;

		GetComponent<AudioSource> ().pitch = pitch;
	}
}
