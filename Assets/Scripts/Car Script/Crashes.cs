﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Crashes : MonoBehaviour {

    private int carHealth;
	private int lapCounter;// Use this for initialization
	private int carID;

	void Start () {
        carHealth = 100;
		lapCounter = 0;
		carID = 1;
	}

    void Update()
    {

    }
	void onTriggerEnter(Collider other)
	{
        if (other.gameObject.CompareTag("StartLine"))
        {
            if (lapCounter == 2)
            {
                SceneManager.LoadScene(2);
            }
            else
                lapCounter++;
        }
        else if (other.gameObject.CompareTag("Railing"))
        {
            carHealth += 10;
            if (carHealth == 0)
            {
                int scene = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(scene, LoadSceneMode.Single);
            }
        }
	}
}
