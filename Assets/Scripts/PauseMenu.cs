﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public GameObject pauseMenu;
	public Button pauseButton;
	public GameObject HUD;
	public GameObject ResultScreen;
	public Text ResultScreenText;
	public Laps playerLaps;
	public Laps AILaps; // cpu racers

	private float timeElapsed;
	private int minutes;
	private float seconds;
	public Text timerDisplay;
	public Text timerResults;

	// Use this for initialization
	void Start () {
		pauseMenu.SetActive (false);
		ResultScreen.SetActive (false);
		Time.timeScale = 1.0F;
		timerDisplay.text = "0:00.00";
	}
	
	// Update is called once per frame
	void Update () {
		if (playerLaps.Lap > 0 && (playerLaps.Lap <= playerLaps.MaxLapNumber)) {
			timeElapsed += Time.deltaTime;
			minutes = Mathf.FloorToInt (timeElapsed / 60);
			seconds = Mathf.Round ((timeElapsed % 60) * 100f) / 100f;
			timerDisplay.text = minutes.ToString () + ":";
			if (seconds < 10)
				timerDisplay.text += "0";
			timerDisplay.text += seconds.ToString ("F");
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
				pauseButton.onClick.Invoke (); }
		if (playerLaps.Lap > playerLaps.MaxLapNumber) {
			endRace ();
		}
	}

	public void PauseButton() {
		Time.timeScale = 0.0F;
	}

	public void ResumeButton(){
		Time.timeScale = 1.0F;
	}

	public void RestartLevel(){
		SceneManager.LoadSceneAsync (SceneManager.GetActiveScene().name);
	}

	public void ReturnToMenu(){
		SceneManager.LoadSceneAsync ("TitleScreen");
	}

	public void endRace(){
		HUD.SetActive (false);
		ResultScreen.SetActive (true);
		ResultScreenText.text = "Race Results";
		timerResults.text = minutes.ToString () + ":";
		if (seconds < 10)
			timerResults.text += "0";
		timerResults.text += seconds.ToString ("F");
		Time.timeScale = 0.0f;
	}

}
