﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CarEngine : MonoBehaviour {

	public Transform path;
	public float maxSteerAngle;
	public float turnSpeed; 
	public WheelCollider wheelFL;
	public WheelCollider wheelFR;
	public WheelCollider wheelRL;
	public WheelCollider wheelRR;
	public float maxMotorToruqe;
	public float currentSpeed;
	public float maxSpeed;
	public Vector3 centerOfMass;
	//public bool isBraking = false;
	public Texture2D textureNormal;
	public Texture2D textureBraking;
	public Renderer carRenderer;
	public float maxBrakeTorque;
	
	[Header("Sensors")]
	public float sensorLength;
	public float sensorLength2;
	public Vector3 frontSensorPosition = new Vector3(-1.4f, 0f ,0f);
	public float frontSideSensorPosition = 1.4f;
	public float frontSensorAngle = 30f;
	private float targetSteerAngle = 0;

	private bool isBraking = false;
	private List<Transform> nodes;
	private int currentNode = 0; 
	private bool avoiding = false;

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().centerOfMass = centerOfMass;
		Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
		nodes = new List<Transform>();	

		for(int i = 0; i < pathTransforms.Length ; i++)
		{
			if(pathTransforms[i] != path.transform)
			{
				nodes.Add(pathTransforms[i]);
			}
		}

	}
	
	// Update is called once per frame
	private void FixedUpdate () {
		Sensors();
		ApplySteer();
		Drive();
		CheckWaypointDistance();
		Braking();
		LerpToSteerAngle();
	}

	private void Sensors()
	{
		RaycastHit hit;
		Vector3 sensorStartPos = transform.position;
		sensorStartPos += transform.forward * frontSensorPosition.z;
		sensorStartPos += transform.up * frontSensorPosition.y;
		float avoidMultiplier = 0;
		avoiding = false;
	
	
		//front right sensor
		sensorStartPos += transform.right * frontSideSensorPosition;
		if(Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
		{
			if(!hit.collider.CompareTag("Track") && !hit.collider.CompareTag("Brake") && !hit.collider.CompareTag("Finish")){
			Debug.DrawLine(sensorStartPos, hit.point);
			avoiding = true;
			avoidMultiplier -= 3f;}
		}

		//front right angle sensor
		else if(Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength2))
		{
			if(!hit.collider.CompareTag("Track") && !hit.collider.CompareTag("Brake") && !hit.collider.CompareTag("Finish")){
			Debug.DrawLine(sensorStartPos, hit.point);
			avoiding = true;
			avoidMultiplier -= 0.8f;
			}

		}
	
		//front left sensor
		sensorStartPos -= transform.right * frontSideSensorPosition * 2;
		if(Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength2))
		{
			if(!hit.collider.CompareTag("Track") && !hit.collider.CompareTag("Brake") && !hit.collider.CompareTag("Finish")){
			Debug.DrawLine(sensorStartPos, hit.point);
			avoiding = true;
			avoidMultiplier += 3f;}
		
		}
		 
		//front left angle sensor
		else if(Physics.Raycast(sensorStartPos, Quaternion.AngleAxis(-frontSensorAngle, transform.up) * transform.forward, out hit, sensorLength))
		{
			if(!hit.collider.CompareTag("Track") && !hit.collider.CompareTag("Brake") && !hit.collider.CompareTag("Finish")){
			Debug.DrawLine(sensorStartPos, hit.point);
			avoiding = true;
			avoidMultiplier += 0.8f;}
			

		}
		
		//front center sensor
		if(avoidMultiplier == 0f)
		{
		if(Physics.Raycast(sensorStartPos, transform.forward, out hit, sensorLength))
		{
			if(!hit.collider.CompareTag("Track") && !hit.collider.CompareTag("Finish")){
			Debug.DrawLine(sensorStartPos, hit.point);
			avoiding = true;
			}
		}
		if(hit.normal.x < 0)
		{
			avoidMultiplier = -1;
		}
		else{
			avoidMultiplier = 1;
		}
		}	
		if(avoiding)
		{
			targetSteerAngle = maxSteerAngle * avoidMultiplier;
		}
	}
	private void ApplySteer()
	{
		if(avoiding) return;
		Vector3 relativeVector = transform.InverseTransformPoint(nodes[currentNode].position);
		relativeVector /= relativeVector.magnitude;
		float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
		targetSteerAngle = newSteer;
		
	}

	private void Drive()
	{
		currentSpeed = 2 * Mathf.PI * wheelFL.radius * wheelFR.rpm * 60 / 1000;
		if(currentSpeed < 80){
			isBraking = false;			
			wheelFL.motorTorque = wheelFR.motorTorque = maxMotorToruqe;}
		else{
			isBraking = true;
			wheelFL.motorTorque = wheelFR.motorTorque = 0f;}
	}

	private void CheckWaypointDistance()
	{
		if(Vector3.Distance(transform.position , nodes[currentNode].position) < 7f)
		{
			if(currentNode == nodes.Count -1)
			{currentNode = 0;}
			else
			{currentNode++;}
		}
	}
	private void Braking()
	{
	if(isBraking){
		carRenderer.material.mainTexture = textureBraking;
		wheelRL.brakeTorque = wheelRR.brakeTorque = maxBrakeTorque;
	}
	else{
		carRenderer.material.mainTexture = textureNormal;
		wheelRL.brakeTorque = wheelRR.brakeTorque = 0;
	}
	
	}
	
	private void LerpToSteerAngle()
	{
		wheelFL.steerAngle = Mathf.Lerp(wheelFL.steerAngle , targetSteerAngle , Time.deltaTime * turnSpeed);
		wheelFR.steerAngle = Mathf.Lerp(wheelFR.steerAngle , targetSteerAngle , Time.deltaTime * turnSpeed);
	}
}
