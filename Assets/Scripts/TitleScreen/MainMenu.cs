﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public GameObject levelMenu;
	public GameObject loadingOverlay;

	// Use this for initialization
	void Start () {
		levelMenu.SetActive (false);
		loadingOverlay.SetActive (false);
	}

	public void startPracticeTrack()
	{
		loadingOverlay.SetActive (true);
		SceneManager.LoadSceneAsync ("PracticeTrack");
	}
		
	public void startLevel1()
	{
		loadingOverlay.SetActive (true);
		SceneManager.LoadSceneAsync ("Alejandros Final Scene");
	}

	public void startLevel2()
	{
		loadingOverlay.SetActive (true);
		SceneManager.LoadSceneAsync ("Level2");
	}

	// Update is called once per frame
	void Update () {
		
	}
}
