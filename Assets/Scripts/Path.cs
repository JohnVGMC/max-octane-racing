﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class Path : MonoBehaviour {

	public Color lineColor;
	private List<Transform> nodes =  new List<Transform>();
void OnDrawGizmosSelected(){
	Gizmos.color = lineColor;


	Transform[] pathTransforms = GetComponentsInChildren<Transform>();
	nodes = new List<Transform>();	

	for(int i = 0; i < pathTransforms.Length ; i++)
	{
		if(pathTransforms[i] != transform)
		{
			nodes.Add(pathTransforms[i]);
		}
	}

	for(int i = 0; i < nodes.Count; i++)
	{
		Vector3 currentNode = nodes[i].position;
		Vector3 previousNode = Vector3.zero;
		previousNode = nodes[(nodes.Count - 1 + i)% nodes.Count].position;
		
		Gizmos.DrawLine(previousNode, currentNode);
		Gizmos.DrawWireSphere(currentNode, 0.3f);
	}


}
}
